import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {ServerService} from "../server.service";
import {ServerStatus} from "../server-status";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: "server-list",
  templateUrl: "./server-list.component.html",
  styleUrls: ["./server-list.component.css"]
})
export class ServerListComponent implements OnInit {
  @Input() serverList: ServerStatus[];
  @Output() logoutEvent: EventEmitter<any> = new EventEmitter<any>();
  public lastModified = "";
  public waitingForUpdate = false;
  constructor(private serverService: ServerService) {
  }

  startServer(server: ServerStatus) {
    const index = this.serverList.findIndex( value => value === server);
    server.statusCode = 2;
    this.serverService.startServer(server.name).subscribe(answer => {
      this.serverList[index] = answer;
    }, (error: HttpErrorResponse) => {
      this.serverList[index].status = error.message;
      this.serverList[index].statusCode = error.status;
    });
  }

  stopServer(server: ServerStatus) {
    const index = this.serverList.findIndex( value => value === server);
    server.statusCode = 2;
    this.serverService.stopServer(server.name).subscribe(answer => {
      this.serverList[index] = answer;
    }, (error: HttpErrorResponse) => {
      this.serverList[index].status = error.message;
      this.serverList[index].statusCode = error.status;
    });
  }

  refresh(): void {
    this.waitingForUpdate = true;
    this.serverService.getAndUpdateServerList().subscribe(servers => {
      this.serverList = servers;
      this.setLastUpdate();
      this.waitingForUpdate = false;
    }, (error: HttpErrorResponse) => {
      this.serverList = [
        new ServerStatus(
          "error", "error", "error", "error", error.message, error.status, ["error"], "NONE"
        )
      ];
      this.waitingForUpdate = false;
    });
  }

  setLastUpdate(): void {
    const date = new Date();
    const dd = String(date.getDate()).padStart(2, "0");
    const mm = String(date.getMonth() + 1).padStart(2, "0"); // January is 0!
    const yyyy = date.getFullYear();
    const min = String(date.getMinutes()).padStart(2, "0");
    const sec = String(date.getSeconds()).padStart(2, "0");
    const hh = String(date.getHours()).padStart(2, "0");

    this.lastModified = `Zuletzt aktualisiert: ${dd}.${mm}.${yyyy} ${hh}:${min}:${sec}`;
  }

  getUserName(): string {
    return this.serverService.userName;
  }

  logout(): void {
    this.serverService.logout();
    this.logoutEvent.emit(true);
  }

  ngOnInit() {
    this.refresh();
  }
}
