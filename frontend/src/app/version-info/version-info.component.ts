import { Component, OnInit } from "@angular/core";
import {ServerService} from "../server.service";

@Component({
  selector: "app-version-info",
  templateUrl: "./version-info.component.html",
  styleUrls: ["./version-info.component.css"]
})
export class VersionInfoComponent implements OnInit {
  apiVersion: string;
  frontendVersion = "2021-07-29.3";
  backendVersion: string;

  constructor(private readonly serverService: ServerService) { }

  ngOnInit(): void {
    this.serverService.getVersion().subscribe(json => {
      this.apiVersion = json.version;
      this.backendVersion = json.backend;
    });
  }

}
