import {Component, EventEmitter} from "@angular/core";
import {ServerStatus} from "./server-status";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "servercontrol";

  public auth = false;
  public serverStatusList: ServerStatus[];

  setAuth(statusList: ServerStatus[]): void {
    this.serverStatusList = statusList;
    // tslint:disable-next-line:triple-equals
    if (statusList != undefined) {
      this.auth = true;
    }
  }

  logout(): void {
    this.auth = false;
  }
}
