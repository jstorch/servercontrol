export class ServerStatus {
  constructor(
    public name: string,
    public ipv4: string,
    public ipv6: string,
    public domain: string,
    public status: string,
    public statusCode: number,
    public statusHover: string[],
    public allowedActions: string
  ) {
  }
}
