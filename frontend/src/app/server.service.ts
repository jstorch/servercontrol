import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ServerStatus} from "./server-status";
import {Observable} from "rxjs";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: "root"
})
export class ServerService {
  private readonly apiUrl: string = environment.apiUrl;
  private authString: string;
  public userName: string;

  constructor(private readonly http: HttpClient) { }

  public getServerList(): Observable<ServerStatus[]> {
    const headers = new HttpHeaders({Authorization: `Basic ${this.authString}`});
    return this.http.get<ServerStatus[]>(`${this.apiUrl}/server?update=false`, {headers});
  }

  public getAndUpdateServerList(): Observable<ServerStatus[]> {
    const headers = new HttpHeaders({Authorization: `Basic ${this.authString}`});
    return this.http.get<ServerStatus[]>(`${this.apiUrl}/server?update=true`, {headers});
  }

  public startServer(server: string): Observable<ServerStatus> {
    const headers = new HttpHeaders({Authorization: `Basic ${this.authString}`});
    return this.http.post(`${this.apiUrl}/server/${server}/start`, "", {headers}) as Observable<ServerStatus>;
  }

  public stopServer(server: string): Observable<ServerStatus> {
    const headers = new HttpHeaders({Authorization: `Basic ${this.authString}`});
    return this.http.post(`${this.apiUrl}/server/${server}/stop`, "", {headers}) as Observable<ServerStatus>;
  }

  public setLogin(username: string, password: string): void {
    this.userName = username;
    this.authString = btoa(`${username}:${password}`);
    localStorage.setItem("AUTH", this.authString);
    localStorage.setItem("USERNAME", this.userName);
  }

  public restoreLogin(): boolean {
    const authString = localStorage.getItem("AUTH");
    const username = localStorage.getItem("USERNAME");
    if (authString != null && username != null) {
      this.authString = authString;
      this.userName = username;
      return true;
    } else {
      return false;
    }
  }

  public logout(): void {
    localStorage.clear();
    this.userName = undefined;
    this.authString = undefined;
  }

  public getVersion(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/version`);
  }
}
