import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { ServerListComponent } from "./server-list/server-list.component";
import {HttpClientModule} from "@angular/common/http";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { LoginComponent } from "./login/login.component";
import {FormsModule} from "@angular/forms";
import {VersionInfoComponent} from "./version-info/version-info.component";

@NgModule({
  declarations: [
    AppComponent,
    ServerListComponent,
    LoginComponent,
    VersionInfoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
  }
}
