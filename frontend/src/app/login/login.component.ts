import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {ServerService} from "../server.service";
import {ServerStatus} from "../server-status";

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {

  public password = "";
  public username = "";
  @Output() readonly auth: EventEmitter<ServerStatus[]> = new EventEmitter<ServerStatus[]>();
  public failed = false;
  public waiting = true;
  public errorMsg = "Login fehlgeschlagen! Wahrscheinlich ist das Passwort falsch!";

  constructor(private serverService: ServerService) { }

  ngOnInit(): void {
    if (this.serverService.restoreLogin()) {
      this.waiting = true;
      this.serverService.getServerList().subscribe(value => {
        this.auth.emit(value);
        this.failed = false;
        this.waiting = false;
      }, error => {
        this.serverService.logout();
        this.failed = false;
        this.waiting = false;
      });
    } else {
      this.waiting = false;
    }
  }

  updateLoginData(): void {
    if (this.password === "" || this.username === "") {
      return;
    }
    this.serverService.setLogin(this.username, this.password);
    this.waiting = true;
    this.serverService.getServerList().subscribe(value => {
      this.auth.emit(value);
      this.failed = false;
      this.waiting = false;
    }, error => {
      this.failed = true;
      this.waiting = false;
      if (error.status === 401) {
        this.errorMsg = "Login fehlgeschlagen! Wahrscheinlich ist das Passwort falsch!";
      } else {
        this.errorMsg = "Serverfehler! Login kann nicht ausgeführt werden!";
      }
    });
  }

  keyPress(event: KeyboardEvent): void {
    if (event.key === "Enter") {
      this.updateLoginData();
    }
  }

}
