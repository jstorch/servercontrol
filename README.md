# Servercontrol Web
The running Website can be found at [servercontrol.jstorch.net](https://servercontrol.jstorch.net)
## Requirements
The Backend needs Java 11 to run and build. The frontend requires nodejs and angular cli for the build.
## Build
Run ``./gradlew build`` to build the backend. \
Run ``ng build --prod`` in the frontend folder to build the frontend. The files can be found in the dist folder.
## Run
To run the backend unpack one of the archives in ``build/distributions`` and execute the servercontrol-web executable in the bin folder. 
## Config
The Backend is configured using a toml config file.
````toml
port = 1234 # serverport

dns.type = "do" # the default dns update type
dns.token = "token" # the default dns update token
dns.baseDomain = "jstorch.net" # the default dns base domain

# list of servers
[[server]]
name = "server1" # servername (must be unique)
type = "doimg" # servertype as specified in net.jstorch.servercontrol.data.ServerTypes
instanceType = "s-2vcpu-4gb" # instance type of the cloud server that will be created
token = "token" # the API Token
dns.type = "do" # overwrite the default dns settings
dns.token = "otherToken"
dns.baseDomain = "example.com"

[[server]]
name = "server2" 
type = "donoimg"
instanceType = "s-1vcpu-2gb"
token = "token"

# list of users
[[user]]
name = "user1" # username
password.salt = "" # password salt that will be appended to the password before hashing
password.hash = "" # the sha512 hashed and salted password b64 encoded
permissions.stop = ["server2"] # list of servers that this user can stop, start and see
permissions.start = ["server1"] # list of servers that this user can start and see
permissions.list = [] # list of servers that this user can see

````
### Supported Server types
#### doimg
[Digital Ocean Server](https://digitalocean.com). Shutdown the server and create a new image that will be loaded on next start
+ requires token, dns type and instanceType
#### donoimg 
[Digital Ocean Server](https://digitalocean.com). Just delete the server without creating a new image.
+ requires token, dns type and instanceType
#### hcloud 
[Hetzner Cloud Server](https://hetzner.de). Shutdown the server and create a new image that will be loaded on next start
+ requires token, dns type and instanceType
### Supported Domain Update Types
#### do 
[Digital Ocean](https://digitalocean.com) Domain
+ requires token and base domain
#### nothing
Don't update a domain
