package net.jstorch.servercontrol

import com.xenomachina.argparser.ArgParser
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.UnauthorizedResponse
import io.javalin.http.queryParamAsClass
import net.jstorch.servercontrol.core.Args
import net.jstorch.servercontrol.core.login.PermissionList
import net.jstorch.servercontrol.core.login.getAllowedServers
import net.jstorch.servercontrol.data.UpdateTypes
import java.time.Instant
import java.util.Timer
import java.util.TimerTask

lateinit var config: Config

fun main(arguments: Array<String>) {
    config = Config(Args(ArgParser(arguments)))

    Javalin.create { config ->
        if (net.jstorch.servercontrol.config.args.enableCors) {
            config.plugins.enableCors { cors ->
                cors.add {
                    it.anyHost()
                }
            }
        }
    }.routes {
        get("version") { ctx ->
            ctx.json(
                mapOf<String, Any>(
                    "version" to "0.4.4",
                    "backend" to "2021-11-23.1",
                    "time" to Instant.now()
                )
            )
        }

        get("server") { ctx ->
            val update = ctx.queryParamAsClass<Boolean>("update").getOrDefault(true)
            val allowedServers = getAllowedServers(ctx, config.userList).filter { perm ->
                perm.permissionLevel != PermissionList.NONE
            }.associateBy { it.server }
            if (allowedServers.isNotEmpty()) {
                ctx.json(
                    if (update) {
                        config.controller.getAndUpdateServerList(allowedServers.keys)
                    } else {
                        config.controller.getServerList(allowedServers.keys)
                    }.onEach { it.allowedActions = allowedServers[it.name]!!.permissionLevel }
                )
            } else {
                throw UnauthorizedResponse()
            }
        }

        get("server/{serverName}") { ctx ->
            val update = ctx.queryParamAsClass<Boolean>("update").getOrDefault(true)
            val allowedServers = getAllowedServers(ctx, config.userList).filter { perm ->
                perm.permissionLevel != PermissionList.NONE && perm.server == ctx.pathParam("serverName")
            }.associateBy { it.server }
            if (allowedServers.isNotEmpty()) {
                ctx.json(
                    if (update) {
                        config.controller.getAndUpdateServerList(allowedServers.keys)[0]
                    } else {
                        config.controller.getServerList(allowedServers.keys)[0]
                    }.apply { allowedActions = allowedServers[name]!!.permissionLevel }
                )
            } else {
                throw UnauthorizedResponse()
            }
        }

        post("server/{serverName}/start") { ctx ->
            val server = ctx.pathParam("serverName")
            val perm = getAllowedServers(ctx, config.userList).find { perm -> perm.server == server }
            if (perm != null && (perm.permissionLevel == PermissionList.START || perm.permissionLevel == PermissionList.STOP)) {
                ctx.json(
                    config.controller.serverUpdate(server, UpdateTypes.START)
                        .apply { allowedActions = perm.permissionLevel }
                )
            } else {
                throw UnauthorizedResponse()
            }
        }

        post("server/{serverName}/stop") { ctx ->
            val server = ctx.pathParam("serverName")
            val perm = getAllowedServers(ctx, config.userList).find { perm -> perm.server == server }
            if (perm != null && perm.permissionLevel == PermissionList.STOP) {
                ctx.json(
                    config.controller.serverUpdate(server, UpdateTypes.STOP)
                        .apply { allowedActions = perm.permissionLevel }
                )
            } else {
                throw UnauthorizedResponse()
            }
        }
    }.start(config.port)
    // set a timer to execute the clean task once per hour
    Timer().schedule(
        object : TimerTask() {
            override fun run() {
                config.controller.clean()
            }
        },
        0,
        3600000L
    )
}
