package net.jstorch.servercontrol

import net.jstorch.servercontrol.cloud.ServerControl
import net.jstorch.servercontrol.cloud.ServerControlDODeleteOnDelete
import net.jstorch.servercontrol.cloud.ServerControlDOImageOnDelete
import net.jstorch.servercontrol.cloud.ServerControlHCImageOnDelete
import net.jstorch.servercontrol.core.Args
import net.jstorch.servercontrol.core.login.PermissionList
import net.jstorch.servercontrol.data.QueryTypes
import net.jstorch.servercontrol.data.Server
import net.jstorch.servercontrol.data.ServerTypes
import net.jstorch.servercontrol.data.User
import net.jstorch.servercontrol.dns.DNSUpdate
import net.jstorch.servercontrol.serverquery.ServerQuery
import net.jstorch.servercontrol.serverquery.minecraft.MinecraftServerQuery
import org.tomlj.Toml
import org.tomlj.TomlArray
import java.nio.file.Paths

/**
 * @author Joshua Storch
 */
class Config(val args: Args) {
    private val toml = Toml.parse(Paths.get(args.tomlPath))
    private val defaultDNSType = toml.getString("dns.type")
    private val defaultDNSToken = toml.getString("dns.token")
    private val defaultDNSBaseDomain = toml.getString("dns.baseDomain")
    private val defaultDNSResetA = toml.getString("dns.reset.A")
    private val defaultDNSResetAAAA = toml.getString("dns.reset.AAAA")
    val port = toml.getLong("port")?.toInt() ?: 8081
    val userList = createUserList()

    private fun createUserList(): List<User> {
        val list = mutableListOf<User>()
        val users = toml.getArray("user") ?: error("No user defined")
        for (i in 0 until users.size()) {
            val userTable = users.getTable(i)
            val name = userTable.getString("name") ?: error("No username defined")
            val passwordSalt = userTable.getString("password.salt") ?: ""
            val passwordHash = userTable.getString("password.hash") ?: error("No userpassword defined")
            val permissionMap = mutableMapOf<String, PermissionList>()
            userTable.getArray("permissions.list").toStringList().forEach { permissionMap[it] = PermissionList.LIST }
            userTable.getArray("permissions.start").toStringList().forEach { permissionMap[it] = PermissionList.START }
            userTable.getArray("permissions.stop").toStringList().forEach { permissionMap[it] = PermissionList.STOP }
            if (permissionMap.isEmpty()) {
                println("warning: user $name has no permissions")
            }

            list.add(User(name, passwordSalt, passwordHash, permissionMap))
        }
        println("loaded ${list.size} users")
        return list.toList()
    }

    val serverList = createServerList()

    private fun createServerList(): List<Server> {
        val list = mutableListOf<Server>()
        val servers = toml.getArray("server") ?: error("No server defined")
        for (i in 0 until servers.size()) {
            val serverTable = servers.getTable(i)!!
            val serverName = serverTable.getString("name") ?: error("No server name defined for server with index $i")
            val serverToken = serverTable.getString("token")
            val instanceType = serverTable.getString("instanceType")
            val serverType = serverTable.getString("type") ?: error("No server type defined for server $serverName")
            val dnsType = serverTable.getString("dns.type") ?: defaultDNSType
            val dnsToken = serverTable.getString("dns.token") ?: defaultDNSToken
            val dnsBaseDomain = serverTable.getString("dns.baseDomain") ?: defaultDNSBaseDomain
            val dnsResetA = serverTable.getString("dns.reset.A") ?: defaultDNSResetA
            val dnsResetAAAA = serverTable.getString("dns.reset.AAAA") ?: defaultDNSResetAAAA
            val queryType = serverTable.getString("query.type")
            val queryPort = serverTable.getLong("query.port")?.toInt()
            val shutdownOnInactivityEnabled = serverTable.getBoolean("shutdown_on_inactivity.enabled") ?: false
            val shutdownOnInactivityStartTime =
                serverTable.getLocalTime("shutdown_on_inactivity.start_time")
                    ?: if (shutdownOnInactivityEnabled) {
                        error("start_time for shutdown_on_inactivity is required when it is enabled")
                    } else {
                        null
                    }
            val shutdownOnInactivityEndTime =
                serverTable.getLocalTime("shutdown_on_inactivity.end_time")
                    ?: if (shutdownOnInactivityEnabled) {
                        error("end_time for shutdown_on_inactivity is required when it is enabled")
                    } else {
                        null
                    }

            val serverControl: ServerControl = when (serverType) {
                ServerTypes.HETZNER_CLOUD -> {
                    ServerControlHCImageOnDelete(
                        serverToken ?: error("No token defined for server $serverName"),
                        serverName,
                        instanceType ?: error("No instance type defined for server $serverName"),
                        serverTable.getString("location") ?: "nbg1",
                        DNSUpdate.getServerDnsUpdate(dnsType, dnsToken, dnsBaseDomain, dnsResetA, dnsResetAAAA)
                    )
                }
                ServerTypes.DIGITAL_OCEAN_IMAGE_ON_DELETE -> {
                    ServerControlDOImageOnDelete(
                        serverToken ?: error("No token defined for server $serverName"),
                        serverName,
                        DNSUpdate.getServerDnsUpdate(dnsType, dnsToken, dnsBaseDomain, dnsResetA, dnsResetAAAA),
                        instanceType ?: error("No instance type defined for server $serverName")
                    )
                }
                ServerTypes.DIGITAL_OCEAN_DELETE_SERVER -> {
                    ServerControlDODeleteOnDelete(
                        serverToken ?: error("No token defined for server $serverName"),
                        serverName,
                        DNSUpdate.getServerDnsUpdate(dnsType, dnsToken, dnsBaseDomain, dnsResetA, dnsResetAAAA),
                        instanceType ?: error("No instance type defined for server $serverName")
                    )
                }
                else -> error("Unknown Server Type $serverType For Server $serverName")
            }
            val serverQuery: ServerQuery? = when (queryType) {
                QueryTypes.MINECRAFT_QUERY -> MinecraftServerQuery(queryPort = queryPort)
                else -> null
            }

            println("loaded server $i: $serverName, start update")
            list.add(
                Server(
                    serverControl,
                    serverQuery,
                    shutdownOnInactivityEnabled,
                    shutdownOnInactivityStartTime,
                    shutdownOnInactivityEndTime
                )
            )
            println("loaded and updated server $i: $serverName")
        }
        println("loaded ${list.size} servers")
        return list.toList()
    }

    val controller = Controller(serverList)
}

fun TomlArray?.toStringList(): List<String> {
    this ?: return listOf()
    val list = mutableListOf<String>()
    for (i in 0 until size()) {
        list.add(getString(i)!!)
    }
    return list
}
