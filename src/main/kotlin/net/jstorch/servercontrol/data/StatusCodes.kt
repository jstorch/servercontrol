package net.jstorch.servercontrol.data

object StatusCodes {
    const val RUNNING_CANNOT_BE_STOPPED = 3
    const val CAN_BE_STARTED = 0
    const val RUNNING_CAN_BE_STOPPED = 1
    const val WAIT = 2
    const val ERROR = -1
}
