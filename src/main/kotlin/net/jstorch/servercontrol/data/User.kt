package net.jstorch.servercontrol.data

import net.jstorch.servercontrol.core.login.PermissionList

data class User(
    val name: String,
    val passwordSalt: String,
    val passwordHash: String,
    val permissions: Map<String, PermissionList>
)
