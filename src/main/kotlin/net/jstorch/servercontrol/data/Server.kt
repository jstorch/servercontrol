package net.jstorch.servercontrol.data

import net.jstorch.servercontrol.cloud.CleanAfterStop
import net.jstorch.servercontrol.cloud.ServerControl
import net.jstorch.servercontrol.serverquery.ServerQuery
import java.io.IOException
import java.time.Duration
import java.time.Instant
import java.time.LocalTime
import java.util.concurrent.locks.ReentrantLock

data class Server(
    private val serverControl: ServerControl,
    private val serverQuery: ServerQuery?,
    private val shutdownOnInactivityEnabled: Boolean,
    private val shutdownOnInactivityStartTime: LocalTime?,
    private val shutdownOnInactivityEndTime: LocalTime?
) {
    private val mutex = ReentrantLock()
    private var lastStateChangeTime = Instant.MIN
    private var lastUpdateTime = Instant.now()
    var scheduleShutdown: Boolean = false
        private set
    var serverStatus = serverControl.getServerStatus()
        private set

    fun update(): ServerStatus {
        try {
            mutex.lock()
            if (Duration.between(lastUpdateTime, Instant.now()).seconds > serverControl.updateCooldown) {
                serverStatus = serverControl.getServerStatus()
                lastUpdateTime = Instant.now()
                val queryData = queryPlayerData()
                if (queryData != null) {
                    serverStatus.statusHover = queryData.list
                    serverStatus.status = "${queryData.online}/${queryData.max} Spieler online"
                    if (queryData.online > 0) serverStatus.statusCode = StatusCodes.RUNNING_CANNOT_BE_STOPPED
                }
                return serverStatus
            } else {
                throw TooManyRequestsException("Too many updates")
            }
        } finally {
            mutex.unlock()
        }
    }

    fun start(): ServerStatus {
        try {
            mutex.lock()
            if (Duration.between(lastStateChangeTime, Instant.now()).seconds > serverControl.stateChangeCooldown) {
                serverStatus = serverControl.startServer()
                lastStateChangeTime = Instant.now()
                lastUpdateTime = Instant.now()
                return serverStatus
            } else {
                throw TooManyRequestsException("Too many state changes")
            }
        } finally {
            mutex.unlock()
        }
    }

    fun stop(): ServerStatus {
        try {
            mutex.lock()
            if (Duration.between(lastStateChangeTime, Instant.now()).seconds > serverControl.stateChangeCooldown) {
                val onlineCount = queryPlayerData()?.online ?: -1
                if (onlineCount > 0) {
                    throw ShutdownNotAllowedException("There are still $onlineCount clients connected to the server")
                }
                serverStatus = serverControl.stopServer()
                lastStateChangeTime = Instant.now()
                lastUpdateTime = Instant.now()
                return serverStatus
            } else {
                throw TooManyRequestsException("Too many state changes")
            }
        } finally {
            mutex.unlock()
        }
    }

    fun clean(): List<String>? {
        try {
            update()
        } finally {
            if (serverStatus.statusCode == StatusCodes.WAIT && serverControl is CleanAfterStop) {
                try {
                    mutex.lock()
                    return serverControl.clean()
                } finally {
                    mutex.unlock()
                }
            } else if (
                shutdownOnInactivityEnabled &&
                serverStatus.statusCode == StatusCodes.RUNNING_CAN_BE_STOPPED &&
                LocalTime.now().isAfter(shutdownOnInactivityStartTime) &&
                LocalTime.now().isBefore(shutdownOnInactivityEndTime) &&
                queryPlayerData()?.online == 0
            ) {
                return if (scheduleShutdown) {
                    stop()
                    scheduleShutdown = false
                    listOf("Shutting down server due to no connected clients")
                } else {
                    scheduleShutdown = true
                    listOf("Scheduling server shutdown due to no connected clients")
                }
            } else {
                val prevVal = scheduleShutdown
                scheduleShutdown = false
                if (prevVal) {
                    return listOf("Unscheduling server shutdown")
                }
            }
            return null
        }
    }

    fun supportsServerQuery(): Boolean = serverQuery != null

    fun queryPlayerData(): QueryPlayerData? {
        if (
            serverQuery != null &&
            setOf(StatusCodes.RUNNING_CANNOT_BE_STOPPED, StatusCodes.RUNNING_CAN_BE_STOPPED)
                .contains(serverStatus.statusCode) &&
            serverStatus.ipv4.split(".").size == 4
        ) {
            return try {
                serverQuery.getQueryData(serverStatus.ipv4)
            } catch (e: IOException) {
                e.printStackTrace()
                null
            }
        }
        return null
    }
}

class TooManyRequestsException(message: String) : RuntimeException(message)

class ShutdownNotAllowedException(message: String) : RuntimeException(message)
