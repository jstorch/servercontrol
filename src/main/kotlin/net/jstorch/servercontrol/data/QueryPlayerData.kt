package net.jstorch.servercontrol.data

/**
 * @author Joshua
 * @since 26.10.2020
 */
data class QueryPlayerData(
    val online: Int,
    val max: Int,
    val list: List<String>
)
