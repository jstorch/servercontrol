package net.jstorch.servercontrol.data

enum class UpdateTypes {
    START, STOP
}
