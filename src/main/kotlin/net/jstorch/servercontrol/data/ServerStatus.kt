package net.jstorch.servercontrol.data

import net.jstorch.servercontrol.core.login.PermissionList

data class ServerStatus(
    val name: String,
    var ipv4: String,
    var ipv6: String,
    var domain: String?,
    var status: String,
    var statusCode: Int,
    var statusHover: List<String> = listOf(),
    var allowedActions: PermissionList = PermissionList.NONE
)
