package net.jstorch.servercontrol.data

object ServerTypes {
    const val HETZNER_CLOUD = "hcloud"
    const val DOCKER = "docker"
    const val DIGITAL_OCEAN_IMAGE_ON_DELETE = "doimg"
    const val DIGITAL_OCEAN_DELETE_SERVER = "donoimg"
}
