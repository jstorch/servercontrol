package net.jstorch.servercontrol.serverquery.minecraft.data

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
 * @author Joshua
 * @since 26.10.2020
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class MinecraftVersion(
    val name: String,
    val protocol: String
)
