package net.jstorch.servercontrol.serverquery.minecraft.data

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import net.jstorch.servercontrol.data.QueryPlayerData

/**
 * @author Joshua
 * @since 26.10.2020
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class MinecraftStatusResponse(
    val players: MinecraftPlayers,
    val version: MinecraftVersion,
    val favicon: String? = null,
    var time: Int = 0
) {
    fun getQueryPlayerData(): QueryPlayerData = QueryPlayerData(
        players.online,
        players.max,
        players.sample.map { it.name }
    )
}
