package net.jstorch.servercontrol.serverquery.minecraft

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import net.jstorch.servercontrol.data.QueryPlayerData
import net.jstorch.servercontrol.serverquery.ServerQuery
import net.jstorch.servercontrol.serverquery.minecraft.data.MinecraftStatusResponse
import java.io.ByteArrayOutputStream
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.lang.RuntimeException
import java.net.InetSocketAddress
import java.net.Socket
import java.nio.charset.Charset
import java.time.Instant

/**
 * @author Joshua
 * @since 26.10.2020
 */
class MinecraftServerQuery(
    private val timeout: Int = 10000,
    queryPort: Int? = null
) : ServerQuery {
    override val queryPort: Int = queryPort ?: 25565
    private val objectMapper: ObjectMapper = ObjectMapper().registerModules(KotlinModule.Builder().build())

    private fun readVarInt(iS: DataInputStream): Int {
        var i = 0
        var j = 0
        while (true) {
            val k = iS.readByte().toInt()
            i = i or ((k and 0x7F) shl j++ * 7)
            if (j > 5) {
                throw RuntimeException("VarInt too big")
            }
            if (k and 0x80 != 128) {
                break
            }
        }
        return i
    }

    private fun writeVarInt(oS: DataOutputStream, data: Int) {
        var restData = data
        while (true) {
            if ((restData and 0xFFFFFF80.toInt()) == 0) {
                oS.writeByte(restData)
                return
            }

            oS.writeByte(restData and 0x7F or 0x80)
            restData = restData ushr 7
        }
    }

    override fun getQueryData(host: String): QueryPlayerData =
        fetchServerData(InetSocketAddress(host, queryPort)).getQueryPlayerData()

    fun fetchServerData(host: InetSocketAddress): MinecraftStatusResponse {
        val socket = Socket()
        socket.soTimeout = timeout
        socket.connect(host, timeout)

        val outputStream = socket.getOutputStream()
        val dataOutputStream = DataOutputStream(outputStream)

        val inputStream = socket.getInputStream()

        val b = ByteArrayOutputStream()
        val handshake = DataOutputStream(b)
        handshake.writeByte(0x00) // handshake packet id
        writeVarInt(handshake, 4); // version id for handshake
        writeVarInt(handshake, host.hostString.length)
        handshake.writeBytes(host.hostString)
        handshake.writeShort(host.port)
        writeVarInt(handshake, 1) // handshake state 1

        dataOutputStream.write(b.size()) // start stream with packet size
        dataOutputStream.write(b.toByteArray()) // write handshake packet

        dataOutputStream.writeByte(0x01) // size for ping packet
        dataOutputStream.writeByte(0x00) // packet id for ping
        val dataInputStream = DataInputStream(inputStream)
        readVarInt(dataInputStream) // packet size
        var id = readVarInt(dataInputStream) // packet id

        if (id == -1) {
            throw IOException("Premature end of stream.")
        }
        if (id != 0x00) {
            throw IOException("Unexpected packet id")
        }

        val length = readVarInt(dataInputStream) // length of json string
        if (length == -1) {
            throw IOException("Premature end of stream.")
        }
        if (length == 0) {
            throw IOException("Invalid string length.")
        }

        val response = ByteArray(length)
        dataInputStream.readFully(response)
        val jsonString = String(response, Charset.forName("UTF-8"))
        val statusResponse = objectMapper.readValue<MinecraftStatusResponse>(jsonString)

        val currentTime = Instant.now().toEpochMilli()
        dataOutputStream.writeByte(0x09) // packet size
        dataOutputStream.writeByte(0x01) // ping packet id
        dataOutputStream.writeLong(currentTime)

        readVarInt(dataInputStream)
        id = readVarInt(dataInputStream)
        if (id == -1) {
            throw IOException("Premature end of stream.")
        }

        val responseTime = dataInputStream.readLong()
        statusResponse.time = (currentTime - responseTime).toInt()

        dataOutputStream.close()
        outputStream.close()
        dataInputStream.close()
        inputStream.close()
        socket.close()

        return statusResponse
    }
}
