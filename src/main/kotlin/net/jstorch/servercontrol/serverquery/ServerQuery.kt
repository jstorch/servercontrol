package net.jstorch.servercontrol.serverquery

import net.jstorch.servercontrol.data.QueryPlayerData

/**
 * @author Joshua
 * @since 26.10.2020
 */
interface ServerQuery {
    val queryPort: Int
    fun getQueryData(host: String): QueryPlayerData
}
