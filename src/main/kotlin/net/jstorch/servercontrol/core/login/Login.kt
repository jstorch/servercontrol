package net.jstorch.servercontrol.core.login

import io.javalin.http.Context
import net.jstorch.servercontrol.config
import net.jstorch.servercontrol.data.User
import java.nio.charset.Charset
import java.security.MessageDigest
import java.time.Instant
import java.util.Base64

/**
 * @author Joshua Storch
 * get a list of Servers with their permission level for a connection context
 */
fun getAllowedServers(ctx: Context, userList: List<User>): List<ServerPermission> {
    val ip = if (ctx.header("X-Real-IP") != null) {
        ctx.header("X-Real-IP")
    } else {
        ctx.ip()
    }

    // the login information is stored in the Authorization Header
    val auth = ctx.header("Authorization")
    if (auth != null) {
        // the header value is "Basic base64string" and only the base 64 string is needed
        val b64String = auth.split(" ")[1]
        // decode the base 64 string and split the username and the password separated by a :
        val decString = Base64.getDecoder().decode(b64String).toString(Charset.defaultCharset()).split(":")
        val userName = decString[0]
        val user = userList.find { user -> user.name == userName }
        if (user != null) {
            // combine the salt and the password and hash the value
            val saltedPassword = decString[1] + user.passwordSalt
            val hash = MessageDigest.getInstance("SHA-512").digest(saltedPassword.toByteArray())
            // get the password value from the database and compare it with the calculated hash
            val dbPassword = Base64.getDecoder().decode(user.passwordHash)
            if (dbPassword!!.contentEquals(hash)) {
                // get the permission list for the now authenticated user and return it
                if (config.args.accessLog) {
                    println("${Instant.now()}  $ip ${ctx.fullUrl()}: Successful login - $userName")
                }
                return user.permissions.map { ServerPermission(it.key, it.value) }
            } else {
                if (config.args.accessLog) {
                    println("${Instant.now()}  $ip ${ctx.fullUrl()}: Access with wrong password - $userName:${decString[1]}")
                }
            }
        } else {
            if (config.args.accessLog) {
                println("${Instant.now()}  $ip ${ctx.fullUrl()}: Access with wrong Username - $userName")
            }
        }
    } else {
        if (config.args.accessLog) {
            println("${Instant.now()}  $ip ${ctx.fullUrl()}: Access Attempt without auth Header")
        }
    }
    // the login was not successful
    return listOf()
}
