package net.jstorch.servercontrol.core.login

data class ServerPermission(val server: String, val permissionLevel: PermissionList)
