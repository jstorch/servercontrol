package net.jstorch.servercontrol.core.login

enum class PermissionList {
    NONE, LIST, START, STOP
}
