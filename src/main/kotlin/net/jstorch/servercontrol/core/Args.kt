package net.jstorch.servercontrol.core

import com.xenomachina.argparser.ArgParser

class Args(parser: ArgParser) {
    val tomlPath by parser.storing("--conf", help = "The TOML config path")
    val accessLog by parser.flagging("-a", help = "Log all accesses")
    val updateLog by parser.flagging("-u", help = "Log cloudserver update requests")
    val enableCors by parser.flagging("--cors", help = "Enable CORS for development")
}
