package net.jstorch.servercontrol

import io.javalin.http.NotFoundResponse
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import net.jstorch.servercontrol.data.Server
import net.jstorch.servercontrol.data.ServerStatus
import net.jstorch.servercontrol.data.ShutdownNotAllowedException
import net.jstorch.servercontrol.data.StatusCodes
import net.jstorch.servercontrol.data.TooManyRequestsException
import net.jstorch.servercontrol.data.UpdateTypes
import java.time.Instant

/**
 * @author Joshua Storch
 */
class Controller(private val serverList: List<Server>) {
    /**
     * perform the cleaning task for each server in the database
     */
    fun clean() {
        serverList.forEach { server ->
            server.clean()?.let { cleanLog ->
                if (config.args.updateLog) {
                    println("${Instant.now()}: clean: ${server.serverStatus.name}")
                    cleanLog.forEach { log -> println("${Instant.now()}: clean: $log") }
                }
            }
        }
    }

    /**
     * get the Serverstatus for each server that is in the server list and in the allowed servers list
     */
    fun getServerList(allowedServers: Collection<String>): List<ServerStatus> {
        val list = serverList
            .filter { server ->
                allowedServers.contains(server.serverStatus.name)
            }
            .map { it.serverStatus }
        return list
    }

    /**
     * update and return the Serverstatus for each server that is in the server list and in the allowed servers parameter
     * @param allowedServers list of servernames the user is allowed to see the status for
     */
    fun getAndUpdateServerList(allowedServers: Collection<String>): List<ServerStatus> {
        val list = serverList
            .filter { server ->
                allowedServers.contains(server.serverStatus.name)
            }
        // launch each update in an own coroutine and than wait for them to finish
        list.map { server ->
            GlobalScope.launch {
                try {
                    server.update()
                    if (config.args.updateLog) {
                        println("${Instant.now()}: updated ${server.serverStatus.name}")
                    }
                } catch (e: TooManyRequestsException) {
                    if (config.args.updateLog) {
                        println("${Instant.now()}: response from cache for ${server.serverStatus.name}")
                    }
                }
            }
        }.forEach { runBlocking { it.join() } }
        return list.map { it.serverStatus }
    }

    /**
     * start or stop the server
     * @param serverName the server to be started or stopped
     * @param action the action to be executed
     */
    fun serverUpdate(serverName: String, action: UpdateTypes): ServerStatus {
        val server = serverList.find { it.serverStatus.name == serverName } ?: throw NotFoundResponse()

        return when (action) {
            UpdateTypes.START -> {
                try {
                    val status = server.start()
                    if (config.args.updateLog) {
                        println("${Instant.now()}: start ${server.serverStatus.name}")
                    }
                    status
                } catch (e: TooManyRequestsException) {
                    if (config.args.updateLog) {
                        println("${Instant.now()}: too many requests at state change attempt  $serverName")
                    }
                    server.serverStatus
                }
            }
            UpdateTypes.STOP -> {
                try {
                    val status = server.stop()
                    if (config.args.updateLog) {
                        println("${Instant.now()}: stop ${server.serverStatus.name}")
                    }
                    status
                } catch (e: TooManyRequestsException) {
                    if (config.args.updateLog) {
                        println("${Instant.now()}: too many requests at state change attempt  $serverName")
                    }
                    val status = server.serverStatus.copy()
                    status.status = "Zu viele Start-/Stopversuche"
                    status.statusCode = StatusCodes.WAIT
                    status
                } catch (e: ShutdownNotAllowedException) {
                    if (config.args.updateLog) {
                        println("${Instant.now()}: server shutdown not allowed $serverName")
                    }
                    val status = server.serverStatus.copy()
                    status.status = "Noch Spieler online"
                    status.statusCode = StatusCodes.RUNNING_CANNOT_BE_STOPPED
                    status
                }
            }
        }
    }
}
