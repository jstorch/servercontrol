package net.jstorch.servercontrol.cloud

import com.myjeeva.digitalocean.common.DropletStatus
import com.myjeeva.digitalocean.common.ImageStatus
import com.myjeeva.digitalocean.impl.DigitalOceanClient
import com.myjeeva.digitalocean.pojo.Droplet
import com.myjeeva.digitalocean.pojo.Region
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import net.jstorch.servercontrol.data.ServerStatus
import net.jstorch.servercontrol.data.StatusCodes
import net.jstorch.servercontrol.dns.DNSUpdate

class ServerControlDODeleteOnDelete(
    token: String,
    private val serverName: String,
    private val dnsUpdate: DNSUpdate,
    private val instanceType: String
) : ServerControl {
    override val domain: String? = dnsUpdate.baseDomain?.let { "$serverName.$it" }
    override val stateChangeCooldown: Int = 600
    override val updateCooldown: Int = 60
    private val apiClient = DigitalOceanClient(token)
    private val serverSystemName = dnsUpdate.baseDomain?.let { "$serverName-server.$it" } ?: "$serverName-server"
    private val currentImageName = "$serverName-current"

    override fun getServerStatus(): ServerStatus {
        val serverList = apiClient.getAvailableDroplets(0, 20).droplets.filter { droplet ->
            droplet.name == serverSystemName
        }
        if (serverList.size == 1) {
            val server = serverList[0]
            val status = server.status
            val statusCode = if (status == DropletStatus.ACTIVE) {
                StatusCodes.RUNNING_CAN_BE_STOPPED
            } else {
                StatusCodes.WAIT
            }
            return ServerStatus(
                serverName,
                server.networks.version4Networks.filter { !it.ipAddress.startsWith("10.") }.getOrNull(0)?.ipAddress ?: "noch nicht verfügbar",
                server.networks.version6Networks.getOrNull(0)?.ipAddress ?: "noch nicht verfügbar",
                domain,
                "Serverstatus: ${status.name}",
                statusCode
            )
        } else if (serverList.isEmpty()) {
            val imageList = apiClient.getAvailableSnapshots(0, 20).snapshots.filter { image ->
                image.name == currentImageName
            }
            if (imageList.size == 1) {
                return ServerStatus(serverName, "", "", domain, "Ausgeschaltet", StatusCodes.CAN_BE_STARTED)
            }
        }
        return ServerStatus(serverName, "error", "error", domain, "error", StatusCodes.ERROR)
    }

    override fun startServer(): ServerStatus {
        val serverList = apiClient.getAvailableDroplets(0, 20).droplets.filter { droplet ->
            droplet.name == serverSystemName
        }
        if (serverList.size == 1) {
            val server = serverList[0]
            val statusCode = if (server.status == DropletStatus.ACTIVE) {
                StatusCodes.RUNNING_CAN_BE_STOPPED
            } else {
                StatusCodes.WAIT
            }
            ServerStatus(
                serverName,
                server.networks.version4Networks.filter { !it.ipAddress.startsWith("10.") }.getOrNull(0)?.ipAddress ?: "noch nicht verfügbar",
                server.networks.version6Networks.getOrNull(0)?.ipAddress ?: "noch nicht verfügbar",
                domain,
                "Serverstatus: ${server.status}",
                statusCode
            )
        } else if (serverList.size > 1) {
            return ServerStatus(serverName, "error", "error", domain, "error", StatusCodes.ERROR)
        } else if (serverList.isEmpty()) {
            val imageList = apiClient.getUserImages(0, 20).images.filter { image ->
                image.name == currentImageName
            }
            if (imageList.isEmpty()) {
                return ServerStatus(serverName, "", "", domain, "Kein Image", StatusCodes.ERROR)
            }
            if (imageList[0].status != ImageStatus.AVAILABLE) {
                return ServerStatus(serverName, "", "", domain, "Image Status: ${imageList[0].status}", StatusCodes.WAIT)
            }
            val newServer = Droplet()
            newServer.name = serverSystemName
            newServer.size = instanceType
            newServer.region = Region("fra1")
            newServer.image = imageList[0]
            newServer.enableIpv6 = true
            newServer.enableBackup = false
            newServer.enablePrivateNetworking = false
            newServer.keys = apiClient.getAvailableKeys(0).keys
            apiClient.createDroplet(newServer)

            GlobalScope.launch {
                delay(30000)
                updateDNS()
            }

            // ein gerade erstellter server hat noch keine netzwerke
            // val ipv4 = createdServer.networks.version4Networks[0].ipAddress
            // val ipv6 = createdServer.networks.version6Networks[0].ipAddress
            // updateDNS(ipv4, ipv6)
            return ServerStatus(serverName, "noch nicht verfügbar", "noch nicht verfügbar", domain, "Server startet", StatusCodes.WAIT)
        }

        return ServerStatus(serverName, "error", "error", domain, "error", StatusCodes.ERROR)
    }

    override fun stopServer(): ServerStatus {
        val serverList = apiClient.getAvailableDroplets(0, 20).droplets.filter { droplet ->
            droplet.name == serverSystemName
        }
        if (serverList.isEmpty()) {
            return ServerStatus(serverName, "", "", domain, "kein Server", StatusCodes.ERROR)
        } else if (serverList.size > 1) {
            return ServerStatus(serverName, "error", "error", domain, "error", StatusCodes.ERROR)
        } else {
            val server = serverList[0]
            return if (server.status == DropletStatus.ACTIVE) {
                apiClient.deleteDroplet(server.id)
                dnsUpdate.resetDNS(serverName)
                ServerStatus(serverName, "", "", domain, "Wird gelöscht", StatusCodes.WAIT)
            } else {
                ServerStatus(
                    serverName,
                    server.networks.version4Networks.filter { !it.ipAddress.startsWith("10.") }[0].ipAddress,
                    server.networks.version6Networks[0].ipAddress,
                    domain,
                    "Serverstatus: ${server.status.name}",
                    StatusCodes.WAIT
                )
            }
        }
    }

    private suspend fun updateDNS() {
        var servers = apiClient.getAvailableDroplets(0, 20).droplets.filter { droplet ->
            droplet.name == serverSystemName
        }
        var errorCounter = 0
        while (
            servers.isEmpty() ||
            servers[0].networks.version4Networks.filter { !it.ipAddress.startsWith("10.") }.isEmpty() ||
            servers[0].networks.version6Networks.isEmpty()
        ) {
            delay(10000)
            servers = apiClient.getAvailableDroplets(0, 20).droplets.filter { droplet ->
                droplet.name == serverSystemName
            }
            errorCounter++
            if (errorCounter > 20) {
                error("DNS Update for $serverName not successful")
            }
        }

        dnsUpdate.updateDNS(
            serverName,
            servers[0].networks.version4Networks.filter { !it.ipAddress.startsWith("10.") }[0].ipAddress,
            servers[0].networks.version6Networks[0].ipAddress
        )
    }
}
