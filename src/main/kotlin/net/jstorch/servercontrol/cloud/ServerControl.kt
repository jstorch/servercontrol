package net.jstorch.servercontrol.cloud

import net.jstorch.servercontrol.data.ServerStatus

/**
 * @author Joshua Storch
 */
interface ServerControl {
    /**
     * subdomain of the server
     */
    val domain: String?

    /**
     * delay between two updates for a server of this type
     */
    val updateCooldown: Int

    /**
     * delay between two state changes (start/stop) for a server of this type
     */
    val stateChangeCooldown: Int

    /**
     * get the server Status
     */
    fun getServerStatus(): ServerStatus

    /**
     * start the server
     */
    fun startServer(): ServerStatus

    /**
     * stop the server
     */
    fun stopServer(): ServerStatus
}
