package net.jstorch.servercontrol.cloud

/**
 * @author Joshua Storch
 * @since 13.10.2020
 * interface that indicates that a clean action has to be performed after the server has been stopped from the website
 */
interface CleanAfterStop {
    fun clean(): List<String>
}
