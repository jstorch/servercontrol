package net.jstorch.servercontrol.cloud

import me.tomsdevsn.hetznercloud.HetznerCloudAPI
import me.tomsdevsn.hetznercloud.objects.request.ChangeReverseDNSRequest
import me.tomsdevsn.hetznercloud.objects.request.CreateImageRequest
import me.tomsdevsn.hetznercloud.objects.request.ServerRequest
import me.tomsdevsn.hetznercloud.objects.request.UpdateImageRequest
import me.tomsdevsn.hetznercloud.objects.response.ImageType
import net.jstorch.servercontrol.data.ServerStatus
import net.jstorch.servercontrol.data.StatusCodes
import net.jstorch.servercontrol.dns.DNSUpdate
import java.util.concurrent.TimeUnit

class ServerControlHCImageOnDelete(
    token: String,
    private val serverName: String,
    private val instanceType: String,
    private val serverLocation: String,
    private val dnsUpdate: DNSUpdate
) : ServerControl, CleanAfterStop {
    private val cloud: HetznerCloudAPI = HetznerCloudAPI(token)
    private val serverSystemName = dnsUpdate.baseDomain?.let { "$serverName.$it" } ?: serverName
    private val oldImageName = "$serverName-old"
    private val currentImageName = "$serverName-current"
    override val domain: String? = dnsUpdate.baseDomain?.let { "$serverName.$it" }
    override val stateChangeCooldown: Int = 600
    override val updateCooldown: Int = 60

    /**
     * get Server Status Information
     */
    override fun getServerStatus(): ServerStatus {
        val serverList = cloud.getServerByName(serverSystemName).servers
        if (serverList.size == 1) {
            val server = serverList[0]
            val status = server.status
            val imageList = cloud.getImages(ImageType.SNAPSHOT).images.filter { img ->
                img.description == currentImageName
            }
            return if (imageList.isNotEmpty()) {
                ServerStatus(
                    serverName,
                    server.publicNet.ipv4.ip,
                    server.publicNet.ipv6.ip.replace("/64", "1"),
                    domain,
                    "Wird gelöscht",
                    StatusCodes.WAIT
                )
            } else {
                val statusCode = if (status == "running") {
                    StatusCodes.RUNNING_CAN_BE_STOPPED
                } else {
                    StatusCodes.WAIT
                }
                ServerStatus(
                    serverName,
                    server.publicNet.ipv4.ip,
                    server.publicNet.ipv6.ip.replace("/64", "1"),
                    domain,
                    "Serverstatus: $status",
                    statusCode
                )
            }
        } else if (serverList.isEmpty()) {
            val imageList = cloud.getImages(ImageType.SNAPSHOT).images.filter { img ->
                img.description == currentImageName
            }
            if (imageList.size == 1) {
                return ServerStatus(serverName, "", "", domain, "Ausgeschaltet", StatusCodes.CAN_BE_STARTED)
            }
        }
        return ServerStatus(serverName, "error", "error", domain, "error", StatusCodes.ERROR)
    }

    /**
     * if the server does not exist it will be started, if it exists the status will be returned
     */
    override fun startServer(): ServerStatus {
        val serverList = cloud.getServerByName(serverSystemName).servers
        if (serverList.size == 1) {
            val server = serverList[0]
            val statusCode = if (server.status == "running") {
                StatusCodes.RUNNING_CAN_BE_STOPPED
            } else {
                StatusCodes.WAIT
            }
            ServerStatus(
                serverName,
                server.publicNet.ipv4.ip,
                server.publicNet.ipv6.ip.replace("/64", "1"),
                domain,
                "Serverstatus: ${server.status}",
                statusCode
            )
        } else if (serverList.size > 1) {
            return ServerStatus(serverName, "error", "error", domain, "error", StatusCodes.ERROR)
        } else if (serverList.isEmpty()) {
            val image = cloud.getImages(ImageType.SNAPSHOT).images.filter { img ->
                img.description == currentImageName
            }
            if (image.isEmpty()) {
                return ServerStatus(serverName, "", "", domain, "Kein Image", StatusCodes.ERROR)
            }
            if (image[0].status != "available") {
                return ServerStatus(serverName, "", "", domain, "Image Status: ${image[0].status}", StatusCodes.WAIT)
            }
            val serverRequest = cloud.createServer(
                ServerRequest.builder()
                    .startAfterCreate(true)
                    .serverType(instanceType)
                    .location(serverLocation)
                    .image(image[0].id.toString())
                    .name(serverSystemName)
                    .sshKeys(cloud.sshKeys.sshKeys.map { key -> key.id })
                    .build()
            )
            val server = serverRequest.server
            cloud.updateImage(image[0].id, UpdateImageRequest.builder().description(oldImageName).build())
            val ipv4 = server.publicNet.ipv4.ip
            val ipv6 = server.publicNet.ipv6.ip.replace("/64", "1")
            if (domain != null) {
                cloud.changeDNSPTR(server.id, ChangeReverseDNSRequest.builder().ip(ipv4).dnsPTR(domain).build())
                cloud.changeDNSPTR(server.id, ChangeReverseDNSRequest.builder().ip(ipv6).dnsPTR(domain).build())
            }
            dnsUpdate.updateDNS(serverName, ipv4, ipv6)
            return ServerStatus(serverName, ipv4, ipv6, domain, "Server startet", StatusCodes.WAIT)
        }

        return ServerStatus(serverName, "error", "error", domain, "error", StatusCodes.ERROR)
    }

    /**
     * create a server image so the server will be deleted in the future
     */
    override fun stopServer(): ServerStatus {
        val serverList = cloud.getServerByName(serverSystemName).servers
        if (serverList.isEmpty()) {
            return ServerStatus(serverName, "", "", domain, "kein Server", StatusCodes.ERROR)
        } else if (serverList.size > 1) {
            return ServerStatus(serverName, "error", "error", domain, "error", StatusCodes.ERROR)
        } else {
            val images = cloud.getImages(ImageType.SNAPSHOT).images.filter { img ->
                img.description == currentImageName
            }
            if (images.isNotEmpty()) {
                return ServerStatus(serverName, "", "", domain, "Wird gelöscht", StatusCodes.WAIT)
            }
            val server = serverList[0]
            if (server.status == "running") {
                cloud.shutdownServer(server.id)
                TimeUnit.SECONDS.sleep(6)
                cloud.createImage(
                    server.id,
                    CreateImageRequest.builder().description(currentImageName).type("snapshot").build()
                )

                return ServerStatus(serverName, "", "", domain, "Wird gelöscht", StatusCodes.WAIT)
            } else {
                return ServerStatus(
                    serverName,
                    server.publicNet.ipv4.ip,
                    server.publicNet.ipv6.ip.replace("/64", "1"),
                    domain,
                    "Serverstatus: ${server.status}",
                    StatusCodes.WAIT
                )
            }
        }
    }

    /**
     * delete the old image and delete the server if a new image is found
     */
    override fun clean(): List<String> {
        val serverList = cloud.getServerByName(serverSystemName).servers
        val imageList = cloud.getImages(ImageType.SNAPSHOT).images
        val oldImageList = imageList.filter { it.description == oldImageName }
        val newImageList = imageList.filter { it.description == currentImageName }
        val ret = mutableListOf<String>()
        oldImageList.forEach {
            cloud.deleteImage(it.id)
            ret.add("Old Image deleted")
        }
        if (newImageList.size == 1 && serverList.size == 1 &&
            newImageList[0].status == "available" &&
            serverList[0].status == "off"
        ) {
            cloud.deleteServer(serverList[0].id)
            dnsUpdate.resetDNS(serverName)
            ret.add("Server deleted")
        }
        if (ret.isEmpty()) {
            ret.add("no action performed")
        }
        return ret
    }
}
