package net.jstorch.servercontrol.dns

import net.jstorch.servercontrol.data.DNSUpdateTypes

/**
 * Base interface to implement DNS Updates
 */
interface DNSUpdate {
    /**
     * the base domain for the dns update that will be displayed in the frontend and is used to do the dns update
     */
    val baseDomain: String?

    /**
     * the address the A Record will be reset to on calling resetDNS
     */
    val resetARecord: String?

    /**
     * the address the AAAA Record will be reset to on calling resetDNS
     */
    val resetAAAARecord: String?

    /**
     * reset the DNS Records to the resetA(AAA)Record values after stopping the server
     */
    fun resetDNS(serverName: String) {
        if (resetARecord != null) {
            if (resetAAAARecord != null) {
                updateDNS(serverName, resetARecord!!, resetAAAARecord!!)
            } else {
                updateDNS(serverName, resetARecord!!)
            }
        }
    }

    /**
     * update the ipv4 DNS entry for the server
     */
    fun updateDNS(serverName: String, ipv4: String)

    /**
     * update ipv4 and ipv6 dns entries for the server
     */
    fun updateDNS(serverName: String, ipv4: String, ipv6: String)

    companion object {
        fun getServerDnsUpdate(
            type: String?,
            token: String?,
            baseDomain: String?,
            resetARecord: String?,
            resetAAAARecord: String?
        ): DNSUpdate {
            return when (type ?: error("Undefined DNS Type")) {
                DNSUpdateTypes.DNS_UPDATE_DIGITAL_OCEAN -> DNSUpdateDO(
                    token ?: error("Undefined DNS Token"),
                    baseDomain ?: error("Undefined DNS Base Domain"),
                    resetARecord,
                    resetAAAARecord
                )
                else -> DNSUpdateNoUpdate()
            }
        }
    }
}
