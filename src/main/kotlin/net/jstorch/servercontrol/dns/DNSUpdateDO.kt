package net.jstorch.servercontrol.dns

import com.myjeeva.digitalocean.impl.DigitalOceanClient

/**
 * update class for digital ocean dns records
 */
class DNSUpdateDO(
    token: String,
    override val baseDomain: String,
    override val resetARecord: String? = null,
    override val resetAAAARecord: String? = null
) : DNSUpdate {
    private val apiClient = DigitalOceanClient(token)

    override fun updateDNS(serverName: String, ipv4: String) {
        updateDNS(serverName, ipv4, "")
    }

    override fun updateDNS(serverName: String, ipv4: String, ipv6: String) {
        apiClient.getDomainRecords(baseDomain, 0, 50).domainRecords.forEach { dR ->
            if (dR.name.lowercase() == serverName.lowercase()) {
                when (dR.type) {
                    "A" -> {
                        dR.data = ipv4
                        apiClient.updateDomainRecord(baseDomain, dR.id, dR)
                    }
                    "AAAA" -> {
                        if (ipv6 != "") {
                            dR.data = ipv6
                            apiClient.updateDomainRecord(baseDomain, dR.id, dR)
                        }
                    }
                }
            }
        }
    }
}
