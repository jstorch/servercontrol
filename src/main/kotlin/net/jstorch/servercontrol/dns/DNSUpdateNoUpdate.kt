package net.jstorch.servercontrol.dns

class DNSUpdateNoUpdate : DNSUpdate {
    override val baseDomain: String? = null
    override val resetARecord: String? = null
    override val resetAAAARecord: String? = null

    override fun updateDNS(serverName: String, ipv4: String) {
    }

    override fun updateDNS(serverName: String, ipv4: String, ipv6: String) {
    }
}
